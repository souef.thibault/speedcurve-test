import "../styles/globals.css";
import type { AppProps } from "next/app";
import SpeedCurve from "../utils/speedcurve/speedcurve";
import { SessionProvider } from "next-auth/react";

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}: AppProps) {
  return (
    <SessionProvider session={session}>
      <SpeedCurve luxId="4351534036" />
      <Component {...pageProps} />
    </SessionProvider>
  );
}
