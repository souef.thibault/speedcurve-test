import Link from "next/link";
import styles from "../../styles/Home.module.css";

function NewPage() {
  return (
    <main className={styles.main}>
      <div className={styles.description}>
        <Link href="/">Home</Link>
      </div>
    </main>
  );
}

export default NewPage;
